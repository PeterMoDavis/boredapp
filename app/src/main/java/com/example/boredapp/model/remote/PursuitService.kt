package com.example.boredapp.model.remote

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.migration.DisableInstallInCheck
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET

@Module
@DisableInstallInCheck
interface PursuitService {
    @GET("/api/activity/")
    suspend fun getPursuit(): PursuitDTO
}