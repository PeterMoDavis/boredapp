package com.example.boredapp.model

import com.example.boredapp.model.remote.Pursuit
import com.example.boredapp.model.remote.PursuitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PursuitRepo @Inject constructor(private val pursuitService: PursuitService) {

    suspend fun getPursuit(): Pursuit = withContext(Dispatchers.IO) {
        return@withContext pursuitService.getPursuit()
    }
}