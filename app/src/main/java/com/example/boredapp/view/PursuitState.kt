package com.example.boredapp.view

import com.example.boredapp.model.remote.Pursuit

data class PursuitState(
    var isLoading : Boolean = false,
    val pursuit: Pursuit,
    )