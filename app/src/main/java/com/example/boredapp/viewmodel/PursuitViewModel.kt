package com.example.boredapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.boredapp.model.PursuitRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PursuitViewModel @Inject constructor(private val pursuitRepo: PursuitRepo) : ViewModel() {

    val browsePursuitState = liveData {
        val pursuitResponse = pursuitRepo.getPursuit()
    }

}