package com.example.boredapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BoredHiltApplication : Application() {}